#define a list to hold input
nums = []
while True:
  user_num = input("Please Enter a Number: ")
  if user_num == "":
    break

  #if an error occurs, run the except
  try:
    nums.append(float(user_num))
  except:
    print(f"{user_num} is not a number!")


#find total and average
if len(nums) == 0:
  print("No numbers were given.")
else:
  total = sum(nums)
  average = total / len(nums)

  print(f"Total of all numbers: {total}")
  print(f"Average of all numbers: {average}")